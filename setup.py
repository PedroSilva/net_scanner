
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'name': 'net_monitor',
    'version': '0.0.1',
    'description': 'Rather simple service application that keeps track of the hosts connected to your local area '
                   'network.',
    'author': 'Pedro Silva',
    'url': 'https://gitlab.com/PedroSilva/interest',
    'author_email': 'ams.pedro@gmail.com',
    'license': 'MIT',
    'packages': [],
    'scripts': [],
    'install_requires': [
        'pytest',
        'nmap',
        'sqlalchemy',
    ],
}

setup(**config)
