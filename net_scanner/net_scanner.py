import ConfigParser
import logging
import sys

from sqlalchemy import or_

from model import ORMConnection, ScanEntry, OnlineHost, KnownHost
from scan_processor import execute_scan, list_of_hosts, scan_data, host_data_from_scan, cleaned_scan_data

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
log.addHandler(ch)


_SETTINGS_FILE = '../settings.cfg'


class TooManySimilarHosts(Exception):
    pass


def is_host_known(connection, host_data):
    """
    Checks whether a host is known given the data.

    if it is known returns the host retrieved from the database.
    if it is now known but can be remembered returns a new known host.
    Otherwise returns None and it is regarded as a 'temporary' host.
    """
    candidates = connection.selector(KnownHost).filter(or_(
        KnownHost.addr_mac == host_data.get('addr_mac'),
        KnownHost.hostname == host_data.get('hostname'))
    )

    print 'N candidates : {}'.format(candidates.count())

    if candidates.count() == 1:
        return candidates.first()
    elif candidates.count() == 0:
        return None
    else:
        raise TooManySimilarHosts(
            'There seems to be more than one host with that matches {}.'.format(
                ', '.join([host_data.get('addr_mac'), host_data.get('hostname')])))


def register_scan_data(db_connection, scan_data, scanned_hosts):
    """
    Processes the scan data and ads it to the database.
    """
    # Create scan data entry in the data-base
    scan_entry = ScanEntry(cleaned_scan_data(scan_data))

    n_entries = 0
    new_hosts = 0

    # Process the OnlineHosts creating records for each online host
    for host_data in scanned_hosts:
        ready_data = host_data_from_scan(host_data)
        online_host = OnlineHost(ready_data)
        online_host.scan_entry = scan_entry

        # if Host is known add it to the scan online host entry and save
        maybe_host = is_host_known(db_connection, host_data)
        if maybe_host is not None:
            maybe_host.maybe_update(ready_data)
            online_host.known_host = maybe_host

            log.debug('Known host from {}'.format(ready_data))
        elif host_data.get('hostname') or host_data.get('addr_mac'):
            # Should only consider a new host if there is any information to support it
            # I.e. mac address or hostname, otherwise treat as online host only.
            known_host = KnownHost(ready_data)
            known_host.scan_entries.append(online_host)

            new_hosts += 1
            db_connection.add_object(known_host)

            log.debug('New host from {}'.format(ready_data))

        n_entries += 1
        db_connection.add_object(online_host)

    return n_entries, new_hosts


def process_scan_results(db_connection, scan_result):
    # TODO: Need to define the structure that will be used to populate the data base
    log.info("Processing scan results.")

    scanned_hosts = list_of_hosts(scan_result)
    scanned_data = scan_data(scan_result)

    log.info('{} hosts were detected online in the network.'.format(len(scanned_hosts)))

    # Process scan data
    n_entries, new_hosts = register_scan_data(db_connection, scanned_data, scanned_hosts)
    log.info('{} scan registers and {} new hosts were found and stored.'.format(n_entries, new_hosts))


def get_configurations(config_file=None):
    config_file = config_file or _SETTINGS_FILE

    config = ConfigParser.ConfigParser()
    config.read(config_file)
    return config


def main(argv):
    # Read configuration
    configs = get_configurations()

    # Load db connection according to configuration
    db_connection = ORMConnection.new_connection_for_model(
        configs.get('DataBase', 'connection_string'))

    # perform scan according to configuration
    scan_res = execute_scan('{}/{}'.format(
        configs.get('LocalNetwork', 'ip_v4'), configs.get('LocalNetwork', 'mask_size')
    ), configs.get('Scan', 'scan_arguments'))

    # process scan
    process_scan_results(db_connection, scan_res)

    # Terminating state and loging exit
    log.info(msg='All done, exiting.')


if __name__ == '__main__':
    main(sys.argv)
