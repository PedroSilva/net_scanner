"""
Queries the DB for scan data and produces a JSON file with it. Specifies the date range through a set of
options.

Usage:
  inspector.py
  inspector.py [-h | --help]
  inspector.py [--day=<day>] [--hour=<hour>] [--minutes=<min>]
  inspector.py [--day=<day> --month=<month>] [--hour=<hour>] [--minutes=<min>]
  inspector.py [--date=<day>-<month>-<month>] [--hour=<hour>] [--minutes=<min>]

Options:
  -h --help                       Show this help message and exit
  -D <day> --day=<day>            Select the day (day of month) to the query
  -M <month> --month=<day>        Select month of year to the query
  -H <hour> --hour=<hour>         Select the hour to the query
  -N <min> --minutes=<min>        Select the minutes to the query
  --date=<day>-<month>-<month>    Select the date day, month and year to the query
"""


import sys
import logging
import ConfigParser
import datetime as dt

from model import ORMConnection

from docopt import docopt

from net_scanner import get_configurations
from model import ScanEntry, KnownHost

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
log.addHandler(ch)


class InvalidInputException(Exception):
    pass


def _date_range_from_args(args):
    date_s = args.get('--date')

    right_now = dt.datetime.now()

    if not date_s:
        day_s = int(args.get('--day')) if args.get('--day') else right_now.day
        month_s = int(args.get('--month')) if args.get('--month') else right_now.month
        year_s = int(right_now.year)

    else:
        date_elems_s = args['--date'].split('-')

        if not len(date_elems_s) >= 3:
            raise InvalidInputException('Date argument is not valid: {}'.format(args['--date']))

        day_s = int(date_elems_s[2])
        month_s = int(date_elems_s[1])
        year_s = int(date_elems_s[0])

    hour_s = args.get('--hour')
    min_s = args.get('--minutes')

    hour_e = int(hour_s) if hour_s else 23
    hour_s = int(hour_s) if hour_s else 0

    min_e = int(min_s) + 29 if min_s else 59
    min_s = int(min_s) if min_s else 0

    start_date = dt.datetime(year_s, month_s, day_s, hour_s, min_s)
    end_date = dt.datetime(year_s, month_s, day_s, hour_e, min_e)

    return start_date, end_date


def fetch_all(start_date, end_date):
    get_data = lambda x: x.data

    # Read configuration
    log.info('Loading configuration.')
    configs = get_configurations()

    # Load db connection according to configuration
    log.info('Setting up connection.')
    db_connection = ORMConnection.new_connection_for_model(
        configs.get('DataBase', 'connection_string'))

    log.info('Loading data.')
    scan_entries = map(get_data, db_connection.selector(ScanEntry).filter(
        ScanEntry.time_stamp >= start_date
    ).filter(
        ScanEntry.time_stamp <= end_date
    ).all())
    known_hosts = {x.pop('id'): x for x in map(get_data, db_connection.selector(KnownHost).all())}

    return [scan_entries, known_hosts]


def main(argv):
    print argv
    args = docopt(__doc__, argv=argv[1:])
    print 'Starting ...'
    print args

    start, end = _date_range_from_args(args)
    print ' Start and end are : '
    print start, end

    log.info(msg='Starting ...')

    res = fetch_all(start, end)
    print res

if __name__ == '__main__':
    main(sys.argv)
