import base64
import zlib
import datetime

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

Base = declarative_base()
_Session = sessionmaker()


class DataConnection(object):

    def __init__(self, connect_string):
        self.engine = create_engine(connect_string)
        # self.engine = create_engine(connect_string, echo=True)
        _Session.configure(bind=self.engine)
        self._session = _Session()

    def check_database(self):
        try:
            tmp_connection = self._session.connection()
            tmp_connection.close()
            return True
        except OperationalError:
            return False

    def init_database(self):
        raise NotImplementedError('Init Data base is not implemented in DataConnection base class.')

    @property
    def session(self):
        return self._session


class ORMConnection(DataConnection):

    def __init__(self, connect_string, base):
        super(ORMConnection, self).__init__(connect_string)
        self.base = base

        self.init_database()

    # initialization
    def init_database(self):
        Base.metadata.create_all(self.engine)

    # insert methods
    def add_object(self, new_object):
        self._session.add(new_object)
        self._session.commit()

    def close(self):
        self._session.close()

    @property
    def selector(self):
        return self._session.query

    @classmethod
    def new_connection_for_model(cls, connect_string):
        return cls(connect_string, Base)

    def commit(self):
        self._session.commit()


class InvalidHostException(Exception):
    pass


class ScanEntry(Base):
    """
    Table to store the generic data for each scan.
    """
    __tablename__ = 'scan_entry'
    id = Column(Integer, primary_key=True)
    time_stamp = Column(DateTime)
    elapsed = Column(String)
    up_hosts = Column(Integer)
    total_hosts = Column(Integer)
    scan_command = Column(String)
    scan_info = Column(String)
    hosts = relationship('OnlineHost', back_populates='scan_entry')

    def __init__(self, scan_data):
        timestamp = scan_data['time_stamp']
        timestamp = timestamp if type(timestamp) is datetime.datetime \
            else datetime.datetime.strptime(timestamp.replace('  ', ' '), '%a %b %d %H:%M:%S %Y')

        self.time_stamp = timestamp
        self.elapsed = scan_data['elapsed']
        self.up_hosts = scan_data['up_hosts']
        self.total_hosts = scan_data['total_hosts']
        self.scan_command = scan_data['scan_command']
        self.scan_info = base64.urlsafe_b64encode(zlib.compress(str(scan_data['scan_info'])))

    @property
    def data(self):
        return {
            'time_stamp': self.time_stamp,
            'elapsed': self.elapsed,
            'up_hosts': self.up_hosts,
            'total_hosts': self.total_hosts,
            'scan_command': self.scan_command,
            'scan_info': zlib.decompress(base64.b64decode(self.scan_info)),
            'hosts': map(lambda x: x.data, self.hosts),
            'id': self.id,
        }


class OnlineHost(Base):
    """
    Indicates a moment in time a host is online (creation an NtoN relationship between
    the known_host and the scan_entry.
    """
    __tablename__ = 'online_host'
    id = Column(Integer, primary_key=True)

    # Foreign keys and relationships
    scan_entry_id = Column(Integer, ForeignKey("scan_entry.id"), nullable=False)
    scan_entry = relationship('ScanEntry', back_populates='hosts')
    known_host_id = Column(Integer, ForeignKey("known_host.id"), nullable=True)
    known_host = relationship('KnownHost', back_populates='scan_entries')

    # Extra data
    addr_ipv4 = Column(String)
    open_ports = Column(String)
    whole_thing = Column(String)

    PORT_SEPARATOR = '___'

    def __init__(self, host_dict):
        self.addr_ipv4 = host_dict.get('addr_ipv4', '')
        self.open_ports = self.PORT_SEPARATOR.join(map(str, host_dict.get('open_ports') or []))
        self.whole_thing = base64.urlsafe_b64encode(zlib.compress(str(host_dict)))

    @property
    def ports(self):
        return map(int, filter(bool, self.open_ports.split(self.PORT_SEPARATOR)))

    @property
    def data(self):
        return {
            'ipv4': self.addr_ipv4,
            'ports': self.ports,
            'known_host_id': self.known_host_id,
            'id': self.id,
        }
        

class KnownHost(Base):
    """A user recognized in the scanned network (connected at least once)."""
    HOSTNAMES_SPLIT = '###'

    __tablename__ = 'known_host'
    id = Column(Integer, primary_key=True)
    addr_mac = Column(String)
    hostname = Column(String)
    name = Column(String)
    description = Column(String)

    scan_entries = relationship(
        'OnlineHost', back_populates='known_host'
    )

    def __init__(self, host_dict):
        self.addr_mac = host_dict.get('addr_mac', '')
        t_hostname = host_dict.get('hostname')
        self.hostname = t_hostname if t_hostname is None else \
            self._store_hostname(t_hostname if type(t_hostname) is list else [t_hostname])
        self.name = host_dict.get('name', '')
        self.description = host_dict.get('', '')

    def maybe_update(self, new_data):
        """
        Attempt to update object with new_data, if there is new and different data.
        """
        self.addr_mac = new_data.get('addr_mac') or self.addr_mac
        self.hostname = new_data.get('hostname') or self.hostname
        self.name = new_data.get('name') or self.name
        self.description = new_data.get('description') or self.description

    @classmethod
    def _store_hostname(cls, hostnames):
        return cls.HOSTNAMES_SPLIT.join(sorted(hostnames))

    @property
    def hostnames(self):
        return self.hostname.split(self.HOSTNAMES_SPLIT)

    @property
    def data(self):
        return {
            'addr_mac': self.addr_mac,
            'hostname': self.hostname,
            'name': self.name,
            'description': self.description,
            'id': self.id,
        }
