import sys
import nmap
import logging

import toolz as tz

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
log.addHandler(ch)


class InvalidPathException(Exception):
    pass


def _filter_false(val):
    """ a -> Maybe a """
    return val if val else None


@tz.curry
def _data_navigator(path, data):
    """
    dict(str: a) -> a
    Navigates a json structure given a path and returns the value under that path.
    """
    elem_list = path.split('/')
    try:
        return reduce(lambda m, k: m.get(k), elem_list, data)
    except AttributeError:
        raise InvalidPathException(
            'Path {} is invalid for supplied dictionary.'.format(path)
        )


@tz.curry
def _get_list_of_something(base_path, data):
    """
    Navigates a scan result structure in search of a particular element which is
    expected to have the form of a list and returns it.
    """
    tmp_list = _data_navigator(base_path, data)
    return tmp_list.values()


list_of_hosts = _get_list_of_something('scan')
list_of_tcp_ports = _get_list_of_something('tcp')
scan_data = _data_navigator('nmap')


def execute_scan(scan_scope, scan_arguments):
    """
    Runs the scan assynchronously showing the progress in the log as it goes.
    returns: A dictionary with the result of the scan.
    """
    # - for now lets make this part of the configuration - need to get local ip so as to properly define the scanning addresses
    # - check permissions to run certain scans
    # - what should be the default parameters and why
    # - how will this affect the result and how it needs to be parsed.

    nm = nmap.PortScanner()

    # ip_mask, ip_addr = _fetch_network_info()
    # scan_scope = _make_scan_scope(ip_addr, ip_mask, search_depth)
    # scan_aguments = _make_scan_args(args)

    log.info("Executing scan with arguments: {} {}.".format(scan_scope, scan_arguments))
    return nm.scan(hosts=scan_scope, arguments=scan_arguments)


def cleaned_scan_data(scan):
    return {
        'time_stamp': tz.pipe(scan, _data_navigator('scanstats/timestr'), _filter_false),
        'elapsed': tz.pipe(scan, _data_navigator('scanstats/elapsed'), _filter_false),
        'up_hosts': tz.pipe(scan, _data_navigator('scanstats/uphosts'), _filter_false),
        'total_hosts': tz.pipe(scan, _data_navigator('scanstats/totalhosts'), _filter_false),
        'scan_command': tz.pipe(scan, _data_navigator('command_line'), _filter_false),
        'scan_info': tz.pipe(scan, _data_navigator('scaninfo'), _filter_false),
    }


def host_data_from_scan(host_json):
    return {
        'tinit': None,
        'tend': None,
        'addr_ipv4': tz.pipe(host_json, _data_navigator('addresses/ipv4'), _filter_false),
        'addr_mac': tz.pipe(host_json, _data_navigator('addresses/mac'), _filter_false),
        'open_ports': tz.pipe(host_json, _data_navigator('tcp'), _filter_false),
        'hostname': tz.pipe(host_json, _data_navigator('hostnames'), _filter_false),
    }
