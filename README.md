# Net Scanner

This is a WORK IN PROGRESS project that aims to create and maintain a historical record of devices connected to your home network.
So far I've been able to configure and run the scan execution using numpy.

But there are several bits that I still wish to address:
- Finishing the logic for the network details configuration;
- Finish the tests for the main parts of the application;
- Add a browser tool to query and navigate the results;

If you find this interesting and/or have some advices, please let me know.

Note: This simple project is also meant to enable me to try new technologies and exercise my developer skills. 
Therefore, it is not really relevant to me if there is a tool that already does this same thing or if it does it better.    