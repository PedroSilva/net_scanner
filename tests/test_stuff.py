# import unittest
# import pytest
# import datetime as dt

# from inspector import InvalidInputException
# from inspector import _date_range_from_args, main
# from docopt import DocoptExit


# # testing the command line input parameters
# def test_command_1():
# 	args = ['inspector.py', '--date=2015-7-23', '-H', '13', '-D', '13']

# 	with pytest.raises(DocoptExit):
# 		start, end = main(args)


# def test_command_2():
# 	args = ['inspector.py', '--date=2015-7-23', '-H', '13', '-D', '13', 'M', '1']

# 	with pytest.raises(DocoptExit):
# 		start, end = main(args)


# def test_command_3():
# 	args = ['inspector.py', '-H', '13', '-D', '13', 'D', '1']

# 	with pytest.raises(DocoptExit):
# 		start, end = main(args)


# # testing the corret parsing of the parameters
# def test__date_range_from_args_1():
# 	args = {
# 		'--date': '2015-7-23',
# 		'--day': None,
# 		'--help': False,
# 		'--hour': '13',
# 		'--minutes': '12',
# 		'--month': None,
# 		'inspector.py': True
# 	}

# 	expected = '2015-07-23 13:12:00', '2015-07-23 13:41:00'

# 	start, end = _date_range_from_args(args)
# 	assert str(start), str(end) == expected


# def test__date_range_from_args_2():
# 	args = {
# 		'--date': '',
# 		'--day': '13',
# 		'--help': '8',
# 		'--hour': '13',
# 		'--minutes': '12',
# 		'--month': None,
# 		'inspector.py': True
# 	}

# 	expected = '2015-08-13 13:12:00', '2015-08-13 13:41:00'

# 	start, end = _date_range_from_args(args)
# 	assert str(start), str(end) == expected


# def test__date_range_from_args_3():
# 	args = {
# 		'--date': '',
# 		'--day': 'morangos',
# 		'--help': '8',
# 		'--hour': '13',
# 		'--minutes': '12',
# 		'--month': None,
# 		'inspector.py': True
# 	}

# 	with pytest.raises(ValueError):
# 		start, end = _date_range_from_args(args)


# def test__date_range_from_args_4():
# 	args = {
# 		'--date': '2015-7',
# 		'--day': None,
# 		'--help': False,
# 		'--hour': '13',
# 		'--minutes': '12',
# 		'--month': None,
# 		'inspector.py': True
# 	}

# 	with pytest.raises(InvalidInputException):
# 		start, end = _date_range_from_args(args)
