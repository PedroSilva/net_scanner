"""
Integration tests to validate the required module, its representation and interaction
with the database.
"""

from datetime import datetime

from net_scanner.model import ORMConnection, ScanEntry, KnownHost, OnlineHost


# HOST_SAMPLE_JSON = {
#     'addresses': {
#         'ipv4': '192.168.1.72'
#     },
#     'hostnames': [
#     ],
#     'status': {
#         'reason': 'localhost-response',
#         'state': 'up'
#     },
#     'tcp': {
#         5000: {
#             'conf': '3',
#             'cpe': '',
#             'extrainfo': '',
#             'name': 'upnp',
#             'product': '',
#             'reason': 'syn-ack',
#             'state': 'open',
#             'version': ''
#         }
#     },
#     'vendor': {
#     }
# },
from net_scanner.net_scanner import is_host_known

SCAN_DATA_1 = {
    'time_stamp': datetime(2016, 1, 4, 16, 14, 21),
    'elapsed': '59.7',
    'up_hosts': 5,
    'total_hosts': 256,
    'scan_command': 'yada yada yada ... ',
    'scan_info': None
}

SCAN_DATA_2 = {
    'time_stamp': datetime(2016, 1, 6, 19, 16, 51),
    'elapsed': '65.7',
    'up_hosts': 30,
    'total_hosts': 256,
    'scan_command': 'boda yada tada ... ',
    'scan_info': None
}

HOST_SAMPLE_1 = {
    'addr_ipv4': '192.168.1.23',
    'addr_mac': '43:23:52:A4:5E:1B',
    'hostname': None,
    'open_ports': [22, 80, 88, 5000],
}

HOST_SAMPLE_2 = {
    'addr_ipv4': '192.168.1.42',
    'hostname': None,
    'open_ports': [22, 80, 88, 5000],
}

HOST_SAMPLE_3 = {
    'addr_ipv4': '192.168.1.42',
    'hostname': 'potato',
    'open_ports': [22, 80, 88, 5000],
}

HOST_SAMPLE_4 = {
    'addr_ipv4': '192.168.1.42',
    'addr_mac': '43:23:52:A4:5E:1B',
    'hostname': 'potato',
    'open_ports': [22, 80, 88, 5000, 6001],
}


def validate_query_result(expected_dict, query_obj, exclude=None):
    exclude = exclude or []
    return all([
         v == getattr(query_obj, k) for (k, v) in expected_dict.iteritems()
         if k not in exclude
    ])


# Make a pytest fixture out of this.
def setup_test_db():
    # TODO need to make sure the file doesn't stick around afterwards
    return ORMConnection.new_connection_for_model('sqlite:///:memory:')


def test_db_connection():
    """
    Make sure the expected model is represented in the database.
    Check tables and columns.
    """

    connection = setup_test_db()
    assert len(connection.engine.table_names()) == 3
    assert all(map(
        lambda t_name: t_name in connection.engine.table_names(),
        ['known_host', 'scan_entry', 'online_host']
    ))


def test_adding_scan_entries():
    """
    Test proper insertion and correct retreival of scan entries in the data base
    """

    # start with an empty data base
    connection = setup_test_db()
    assert connection.selector(ScanEntry).count() == 0
    assert connection.selector(KnownHost).count() == 0

    # add host entry to database
    connection.add_object(ScanEntry(SCAN_DATA_1))
    connection.add_object(ScanEntry(SCAN_DATA_2))

    # make sure it was properly saved in the DB
    assert connection.selector(ScanEntry).count() == 2

    # select and validate the results
    data = connection.selector(ScanEntry).first()

    assert data
    assert validate_query_result(SCAN_DATA_1, data, ['scan_info'])


def test_adding_known_hosts():
    """
    Test insertion and proper retrieval of known_host from the data base
    """
    connection = setup_test_db()
    assert connection.selector(KnownHost).count() == 0

    # add host entry to database
    connection.add_object(KnownHost(HOST_SAMPLE_1))

    # make sure it was properly saved in the DB
    assert connection.selector(KnownHost).count() == 1

    data = connection.selector(KnownHost).first()
    assert validate_query_result(HOST_SAMPLE_1, data, ['addr_ipv4', 'open_ports'])


def test_adding_OnlineHost_no_known_hosts():
    """
    Test insertion and query on the OnlineHost.
    No Knownhost.
    """
    connection = setup_test_db()
    assert connection.selector(ScanEntry).count() == 0
    assert connection.selector(KnownHost).count() == 0
    assert connection.selector(OnlineHost).count() == 0

    # Add the data necessary to the test
    scan_entry = ScanEntry(SCAN_DATA_1)
    online_entry = OnlineHost(HOST_SAMPLE_2)

    scan_entry.hosts.append(online_entry)

    connection.add_object(scan_entry)
    connection.add_object(online_entry)

    assert connection.selector(ScanEntry).count() == 1
    assert connection.selector(KnownHost).count() == 0
    assert connection.selector(OnlineHost).count() == 1

    data = connection.selector(OnlineHost).first()

    assert data
    assert validate_query_result(HOST_SAMPLE_2, data, exclude=['addr_mac', 'hostname', 'open_ports'])
    assert data.ports == HOST_SAMPLE_2['open_ports']


def test_hosts_and_scan_entries():
    """
    Test insertion and query on the OnlineHost related to a KnownHost
    scanned_entries separately.
    """
    connection = setup_test_db()
    assert connection.selector(ScanEntry).count() == 0
    assert connection.selector(KnownHost).count() == 0
    assert connection.selector(OnlineHost).count() == 0

    # Add the data necessary to the test
    scan_entry = ScanEntry(SCAN_DATA_1)

    online_entry = OnlineHost(HOST_SAMPLE_1)
    online_entry.scan_entry = scan_entry

    known_host = KnownHost(HOST_SAMPLE_1)
    known_host.scan_entries.append(online_entry)

    # Add objects to the database
    connection.add_object(scan_entry)
    connection.add_object(known_host)

    # Validate database contents
    assert connection.selector(ScanEntry).count() == 1
    assert connection.selector(KnownHost).count() == 1
    assert connection.selector(OnlineHost).count() == 1

    # Extract data and validate contents
    data_host = connection.selector(KnownHost).first()
    data_online = data_host.scan_entries[0]
    data_scan = data_online.scan_entry

    assert validate_query_result(HOST_SAMPLE_1, data_host, ['addr_ipv4', 'open_ports'])
    assert validate_query_result(HOST_SAMPLE_1, data_online, exclude=['addr_mac', 'hostname', 'open_ports'])
    assert data_online.ports == HOST_SAMPLE_1['open_ports']
    assert data_online.scan_entry_id == data_scan.id
    assert data_online.known_host_id == data_host.id
    assert validate_query_result(SCAN_DATA_1, data_scan, ['scan_info'])


def test_is_host_known_mac():
    """
    Validates that mac address can be used to detect a user as knonw
    """
    connection = setup_test_db()
    known_host = KnownHost(HOST_SAMPLE_1)
    connection.add_object(known_host)

    assert is_host_known(connection, {'addr_mac': '43:23:52:A4:5E:1B'})


def test_is_host_known_host_name():
    """
    Validates that host name can be used to detect a user as knonw
    """
    connection = setup_test_db()
    known_host = KnownHost(HOST_SAMPLE_3)
    connection.add_object(known_host)

    assert is_host_known(connection, {'hostname': 'potato'})


def test_is_host_known_no_match():
    """
    Validates the user unknown case
    """
    connection = setup_test_db()
    known_host = KnownHost(HOST_SAMPLE_3)
    connection.add_object(known_host)

    assert is_host_known(connection, {'hostname': 'onion', 'addr_mac': '43:23:52:A4:5E:1B'}) is None


def test_incremental_known_host_data():
    """
    Validates that when new data is available for a known host, it is absorbed
    in the database.
    """
    connection = setup_test_db()
    known_host = KnownHost(HOST_SAMPLE_1)
    connection.add_object(known_host)
    connection.commit()

    assert connection.selector(KnownHost).count() == 1
    known_host_db = connection.selector(KnownHost).all()[0]
    known_host_db.maybe_update(HOST_SAMPLE_4)
    connection.commit()

    assert connection.selector(KnownHost).count() == 1
    res_obj = connection.selector(KnownHost).all()[0]
    assert res_obj.hostname == 'potato'


def test_data_retrieval_full():
    """
    Vanilla test for retrieval of data from the data base.
    Validates that the relationships are maintained after data retrieval.
    """
    connection = setup_test_db()

    # Add the data necessary to the test
    scan_entry = ScanEntry(SCAN_DATA_1)
    online_entry = OnlineHost(HOST_SAMPLE_1)
    online_entry.scan_entry = scan_entry
    known_host = KnownHost(HOST_SAMPLE_1)
    known_host.scan_entries.append(online_entry)

    # Add objects to the database
    connection.add_object(scan_entry)
    connection.add_object(known_host)

    # fetch data from the DB
    scan_ = connection.selector(ScanEntry).all()[0]
    known_ = connection.selector(KnownHost).all()[0]

    assert scan_.data['hosts'][0]['known_host_id'] == known_.data['id']