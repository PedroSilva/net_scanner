import pytest
import os
from net_scanner.scan_processor import InvalidPathException, list_of_hosts, host_data_from_scan


def _get_validate_file_data(fname):
    """Helper function to validate proper reading of existing data from tests data files."""
    with open(fname, "r") as file_data:
        str_data = eval(file_data.read())
        assert str_data
        assert 'nmap' in str_data and str_data['nmap']
        assert 'scanstats' in str_data['nmap'] and str_data['nmap']['scanstats']
        assert 'scan' in str_data and str_data['scan']

        return str_data


def test_host_data_from_scan_no_data():
    """Validating that scan from an empty dictionary returns raises proper exception."""
    with pytest.raises(InvalidPathException):
        host_data_from_scan({})


def _validate_host(host):
    assert host
    assert 'tinit' in host
    assert 'tend' in host
    assert 'addr_ipv4' in host
    assert 'addr_mac' in host
    assert 'open_ports' in host


def test_host_data_from_scan_full_data():
    data = _get_validate_file_data(os.path.dirname(__file__) + '/test_data/demo_scan1.data')

    hosts = list_of_hosts(data)
    assert type(hosts) is list
    assert hosts

    processed_hosts = map(host_data_from_scan, hosts)
    map(_validate_host, processed_hosts)
